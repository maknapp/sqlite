<?php

namespace Maknapp\SQLite;

class QueryJoin
{
    public string $tableA;
    public string $tableB;
    public string $columnA;
    public string $columnB;

    public function __construct(string $tableA, string $tableB, string $columnA, string $columnB)
    {
        $this->tableA = $tableA;
        $this->tableB = $tableB;
        $this->columnA = $columnA;
        $this->columnB = $columnB;
    }

    public function query(): string
    {
        return "LEFT JOIN $this->tableB ON $this->tableA.$this->columnA = $this->tableB.$this->columnB";
    }
}
