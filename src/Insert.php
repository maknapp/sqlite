<?php

namespace Maknapp\SQLite;

use PDO;
use PDOStatement;

class Insert
{
    private Table $table;
    public QueryValues $values;

    public function __construct(Table &$table)
    {
        $this->table = &$table;
        $this->values = new QueryValues();
    }

    public function query(): string
    {
        return "INSERT INTO {$this->table->name}".$this->values->query();
    }

    public function execute(): int|false
    {
        /** @var PDOStatement $statement */
        $statement = $this->table->getDb()->prepare($this->query());
        $this->bindValues($statement);
        $statement->execute();

        if($statement->rowCount() === 0) return false;
        else return $this->table->getDb()->lastInsertId();
    }

    private function bindValues(PDOStatement $statement)
    {
        foreach ($this->values->get() as $item){
            $element = $this->table->getElement($this->table->name.'.'.$item['column']);
            $paramName = $item['column'];
            switch (get_class($element)){
                case "Maknapp\SQLite\ElementInteger":
                    $statement->bindValue($paramName, $item['value'], PDO::PARAM_INT);
                    break;
                case "Maknapp\SQLite\ElementBoolean":
                    $statement->bindValue($paramName, is_bool($item['value']) && $item['value'] ? 1 : 0, PDO::PARAM_BOOL);
                    break;
                case "Maknapp\SQLite\ElementReal":
                case "Maknapp\SQLite\ElementText":
                    $statement->bindValue($paramName, $item['value']);
                    break;
                case "Maknapp\SQLite\ElementBlob":
                    $statement->bindValue($paramName, $item['value'], PDO::PARAM_LOB);
                    break;
                default:
                    throw new ErrorException(get_class($this)." datatype not found");
            }
        }
    }
}
