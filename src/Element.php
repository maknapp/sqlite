<?php

namespace Maknapp\SQLite;

use ErrorException;
use Maknapp\Dialog\Field;
use SimpleXMLElement;

class Element
{
    public     string $name = "";
    protected  bool   $null = true;
    public     bool   $visibility = true;
    public    ?string $type = null;
    public    ?string $label = null;
    protected ?array  $requires = null;
    public     Table  $table;
    public    string  $table_key;
    protected string  $table_value;
    public     mixed  $value = null;
    public    string  $namespace;
    public   Field  $dialogField;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        $this->namespace = $namespace;
        $attributes = $node->attributes();

        if(!is_null($attributes['name'])) $this->name = (string) $attributes["name"];
        else throw new ErrorException('Table element need the attribute name');

        if(!is_null($attributes['null'])) $this->null = (bool) $attributes["null"];
        if(!is_null($attributes['visibility'])) $this->visibility = (string) $attributes["visibility"] !== "false";
        if(!is_null($attributes['type'])) $this->typeCheck((string) $attributes["type"]);
        if(!is_null($attributes['label'])) $this->label = (string) $attributes["label"];
        if(!is_null($attributes['requires'])){
            $fun = function(string $values){
                $req = explode(":", $values);
                return [$this->namespace.'.'.$req[0], $req[1]];
            };
            $this->requires = array_map($fun, explode(";", (string) $attributes['requires']));
        }
    }

    public function getDialogField(array $values): Field
    {
        return $this->dialogField;
    }

    private function typeCheck(string $type){
        if(preg_match('/^(select|link):([a-zA-Z_\\\\]*)\.([a-zA-Z_]*)(?::([a-zA-Z_]*))?$/m', $type, $match)){
            $this->type = $match[1];
            $this->table = new $match[2](); // DBTrait error
            $this->table_key = $match[3];
            if(sizeof($match) > 4) $this->table_value = $match[4];

            if(get_class($this->table->getElement($this->table->name.'.'.$this->table_key)) !== get_class($this)) throw new ErrorException("Maknapp\SQLite\TableElement: linked datatype is not equal to defined datatype");
        }
    }
}
