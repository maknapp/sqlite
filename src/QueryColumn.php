<?php

namespace Maknapp\SQLite;

class QueryColumn
{
    private array $columns = [];

    public function __construct()
    {

    }

    public function add(string $table, string $column){
        $this->columns[] = ['table' => $table, 'column' => $column];
    }

    public function get(): array
    {
        return $this->columns;
    }

    /**
     * @param array<QueryJoin> $joins
     * @return string
     */
    public function query(string $table, array $joins): string
    {
        $tables = [$table];
        foreach ($joins as $join){
            if(!in_array($join->tableA, $tables)) $tables[] = $join->tableA;
            if(!in_array($join->tableB, $tables)) $tables[] = $join->tableB;
        }

        $list = [];
        foreach ($this->columns as $column){
            if(in_array($column['table'], $tables)){
                //count match
                if(preg_match('/count\(.+\)/i', $column['column'])) $list[] = "COUNT(*) AS count";
                else $list[] = $column['table'].'.'.$column['column'].' AS '.$column['table'].'_'.$column['column'];
            }
        }

        return implode(', ', $list);
    }
}
