<?php

namespace Maknapp\SQLite;

use Maknapp\Dialog\Field;
use Maknapp\Dialog\FieldBoolean;
use SimpleXMLElement;

class ElementBoolean extends Element
{
    private  bool $default = false;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['default'])) $this->default = (string) $attributes["default"] !== "false";

        $this->dialogField = new FieldBoolean($namespace, $node);
    }

    public function getDialogField(array $values): Field
    {
        $this->dialogField->value = !is_null($this->value) ? $this->value : $this->default;

        return $this->dialogField;
    }
}
