<?php

namespace Maknapp\SQLite;

use Maknapp\Dialog\Field;
use Maknapp\Dialog\FieldSelect;
use Maknapp\Dialog\FieldText;
use SimpleXMLElement;

class ElementText extends Element
{
    private  int $length = 1;
    private  string $default = "";

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['length'])) $this->length = (int) $attributes["length"];
        if(!is_null($attributes['default'])) $this->default = (string) $attributes["default"];

        if(!is_null($attributes['modus']) && (string) $attributes['modus'] === 'select') $this->dialogField = new FieldSelect($namespace, $node);
        else $this->dialogField = new FieldText($namespace, $node);
    }

    public function getDialogField(array $values): Field
    {
        $this->dialogField->value = !is_null($this->value) ? $this->value : $this->default;

        return $this->dialogField;
    }
}
