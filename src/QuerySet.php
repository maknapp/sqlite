<?php

namespace Maknapp\SQLite;

class QuerySet
{
    private array $columns = [];
    public function __construct()
    {

    }

    public function add(string $table, string $column, $value){
        $this->columns[] = ['table' => $table, 'column' => $column, 'value' => $value];
    }

    public function get(){
        return $this->columns;
    }

    public function query(string $table, array $joins): string
    {
        $tables = [$table];
        foreach ($joins as $join){
            if(!in_array($join->tableA, $tables)) $tables[] = $join->tableA;
            if(!in_array($join->tableB, $tables)) $tables[] = $join->tableB;
        }

        $list = [];
        foreach ($this->columns as $column){
            if(in_array($column['table'], $tables)){
                $list[] = $column['column'].' = :'.$column['table'].'_'.$column['column'];
            }
        }

        if(sizeof($list) > 0) return " SET ".implode(', ', $list);
        else return '';
    }
}
