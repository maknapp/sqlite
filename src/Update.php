<?php

namespace Maknapp\SQLite;

use PDO;
use PDOStatement;

class Update
{
    private Table $table;
    public QueryColumn $column;
    public QueryWhere $where;
    public QuerySet $set;

    public function __construct(Table &$table)
    {
        $this->table = &$table;
        $this->column = new QueryColumn();
        $this->set = new QuerySet();
        $this->where = new QueryWhere();
    }

    public function query(): string
    {
        $query = "UPDATE {$this->table->name}";

        $query .= $this->set->query($this->table->name, $this->table->joins);
        $query .= $this->where->query($this->table->name, $this->table->joins);

        return $query;
    }

    public function execute(): bool
    {
        /** @var PDOStatement $statement */
        $statement = $this->table->getDb()->prepare($this->query());
        $this->bindValues($statement);
        $statement->execute();

        return $statement->rowCount() > 0;
    }

    private function bindValues(PDOStatement $statement)
    {
        foreach (array_merge($this->where->get(), $this->set->get()) as $item){
            if(!isset($item['type']) || $item['type'] !== 2){
                $element = $this->table->getElement($item['table'].'.'.$item['column']);
                $paramName = $item['table'].'_'.$item['column'];
                switch (get_class($element)){
                    case "Maknapp\SQLite\ElementInteger":
                        $statement->bindValue($paramName, $item['value'], PDO::PARAM_INT);
                        break;
                    case "Maknapp\SQLite\ElementBoolean":
                        $statement->bindValue($paramName, is_bool($item['value']) && $item['value'] ? 1 : 0, PDO::PARAM_BOOL);
                        break;
                    case "Maknapp\SQLite\ElementReal":
                    case "Maknapp\SQLite\ElementText":
                        $statement->bindValue($paramName, $item['value']);
                        break;
                    case "Maknapp\SQLite\ElementBlob":
                        $statement->bindValue($paramName, $item['value'], PDO::PARAM_LOB);
                        break;
                    default:
                        throw new ErrorException(get_class($this)." datatype not found");
                }
            }
        }
    }
}
