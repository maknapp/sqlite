<?php

namespace Maknapp\SQLite;

use Maknapp\Dialog\Field;
use Maknapp\Dialog\FieldInteger;
use Maknapp\Dialog\FieldSelect;
use SimpleXMLElement;

class ElementInteger extends Element
{
    private  int $length = 1;
    private  int $default = 0;
    private ?int $min = null;
    private ?int $max = null;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['length'])) $this->length = (int) $attributes["length"];
        if(!is_null($attributes['default'])) $this->default = (int) $attributes["default"];
        if(!is_null($attributes['min'])) $this->min = (int) $attributes["min"];
        if(!is_null($attributes['max'])) $this->max = (int) $attributes["max"];

        if(!is_null($attributes['modus']) && (string) $attributes['modus'] === 'select') $this->dialogField = new FieldSelect($namespace, $node);
        else $this->dialogField = new FieldInteger($namespace, $node);
    }

    public function getDialogField(array $values): Field
    {
        $this->dialogField->value = !is_null($this->value) ? $this->value : $this->default;

        return $this->dialogField;
    }
}
