<?php

namespace Maknapp\SQLite;

use PDO;
use PDOStatement;

class Delete
{
    private Table $table;
    public QueryWhere $where;
    public QueryOrderBy $orderBy;
    public QueryLimit $limit;

    public function __construct(Table &$table)
    {
        $this->table = &$table;
        $this->where = new QueryWhere();
        $this->orderBy = new QueryOrderBy();
        $this->limit = new QueryLimit();
    }

    public function query(): string
    {
        $query = "DELETE FROM {$this->table->name}";

        $query .= $this->where->query($this->table->name, $this->table->joins);
        $query .= $this->orderBy->query($this->table->name, $this->table->joins);
        $query .= $this->limit->query();

        return $query;
    }

    public function execute(): bool
    {
        /** @var PDOStatement $statement */
        $statement = $this->table->getDb()->prepare($this->query());
        $this->bindValues($statement);
        $statement->execute();

        return $statement->rowCount() > 0;
    }

    private function bindValues(PDOStatement $statement)
    {
        foreach ($this->where->get() as $item){
            if($item['type'] !== 2){
                $element = $this->table->getElement($item['table'].'.'.$item['column']);
                $paramName = $item['table'].'_'.$item['column'];
                switch (get_class($element)){
                    case "Maknapp\SQLite\ElementInteger":
                        $statement->bindValue($paramName, $item['value'], PDO::PARAM_INT);
                        break;
                    case "Maknapp\SQLite\ElementBoolean":
                        $statement->bindValue($paramName, isset($item['value']) ? 1 : 0, PDO::PARAM_BOOL);
                        break;
                    case "Maknapp\SQLite\ElementText":
                    case "Maknapp\SQLite\ElementReal":
                        $statement->bindValue($paramName, $item['value']);
                        break;
                    case "Maknapp\SQLite\ElementBlob":
                        $statement->bindValue($paramName, $item['value'], PDO::PARAM_LOB);
                        break;
                    default:
                        throw new ErrorException(get_class($this)." datatype not found");
                }
            }
        }
    }
}
