<?php

namespace Maknapp\SQLite;

use ErrorException;
use Exception;
use Maknapp\Dialog\Dialog;
use PDO;
use PDOStatement;
use SimpleXMLElement;

class Table
{
    protected array $elements = [];
    public string $name;
    public ?array $primary = [];
    private SimpleXMLElement $table;
    public array $joins = [];

    public function __construct(string $path)
    {
        $file = "$path.xml";
        if (file_exists($file)) {
            try {
                $this->table = new SimpleXMLElement(file_get_contents($file));
            } catch (Exception $e) {
                die("Cannot parse $file; Error: $e");
            }
            $this->name = $this->table->attributes()['name'];
            if(!is_null($this->table->attributes()['primary'])){
                $this->primary = array_map(fn (string $column): string => $column, explode(",", $this->table->attributes()['primary']));
            }

            $nodes = $this->table->children();
            foreach ($nodes as $node) {
                $node_name = $this->name.'.'.$node->attributes()['name'];
                $this->elements[$node_name] = match ($node->getName()) {
                    "integer"   => new ElementInteger($this->name, $node),
                    "text"      => new ElementText($this->name, $node),
                    "real"      => new ElementReal($this->name, $node),
                    "blob"      => new ElementBlob($this->name, $node),
                    "boolean"   => new ElementBoolean($this->name, $node),
                    default     => throw new ErrorException(get_class($this) . " Unknown datatype {$node->getName()}"),
                };
            }

            foreach ($this->elements as $element){
                if($element->type === "link" || $element->type === "select"){
                    $this->joins[] = new QueryJoin($element->namespace, $element->table->name, $element->name, $element->table_key);
                }
            }
        }
    }

    public function getElement($key) : Element|null
    {
        if(array_key_exists($key, $this->elements)){
            return $this->elements[$key];
        } else {
            /** @var Element $element */
            foreach ($this->elements as $element){
                if(!is_null($element->table)){
                    if(array_key_exists($key, $element->table->elements)){
                        return $element->table->getElement($key);
                    }
                }
            }
        }
        return null;
    }

    public function getDialogForm(array $values = []): Dialog
    {
        $dialog = new Dialog(name: $this->name);

        /** @var Element $element */
        foreach ($this->elements as $name => $element) {
            if($element->visibility || (in_array($element->name, $this->primary) && array_key_exists($name, $values))){
                if(array_key_exists($name, $values)){
                    if(get_class($element) == "Maknapp\SQLite\ElementBoolean") $element->value = $values[$name] === "1";
                    else $element->value = $values[$name];
                }
                $dialog->appendElement($element->getDialogField($values));
            }
        }
        return $dialog;
    }

    public function saveForm($update = null): int|bool
    {
        // replace first _ through . in $_POST and $_FILES
        foreach($_POST as $key => $value) $_POST[replace_first_str('_', '.', $key)] = $value;
        foreach($_FILES as $key => $value) $_FILES[replace_first_str('_', '.', $key)] = $value;

        if(is_null($update)){
            $update = true;
            foreach ($this->primary as $prim){
                if(!(isset($_POST[$prim]) && $_POST[$prim] !== "")){
                    $update = false;
                    break;
                }
            }
        }

        if($update){
            $SQLUpdate = new Update($this);
            foreach ($this->elements as $name => $element) {
                if(!in_array($element->name, $this->primary)){
                    switch (get_class($element)){
                        case 'Maknapp\SQLite\ElementBoolean':
                            $SQLUpdate->set->add($this->name, $element->name, isset($_POST[$name]));
                            break;
                        case 'Maknapp\SQLite\ElementBlob':
                            if(!empty($_FILES[$name]) && filesize($_FILES[$name]['tmp_name']) > 0)
                                $SQLUpdate->set->add($this->name, $element->name, file_get_contents($_FILES[$name]['tmp_name']));
                            break;
                        default:
                            $SQLUpdate->set->add($this->name, $element->name, $_POST[$name]);
                            break;
                    }
                }
            }
            foreach ($this->primary as $prim){
                $SQLUpdate->where->add($this->name, $prim, $_POST["$this->name.$prim"]);
            }

            return $SQLUpdate->execute();
        } else {
            $SQLInsert = new Insert($this);
            foreach ($this->elements as $name => $element) {
                if($element->visibility || array_key_exists($name, $_POST)) {
                    switch (get_class($element)){
                        case 'Maknapp\SQLite\ElementBoolean':
                            $SQLInsert->values->add($element->name, isset($_POST[$name]));
                            break;
                        case 'Maknapp\SQLite\ElementBlob':
                            if(!empty($_FILES[$name]) && filesize($_FILES[$name]['tmp_name']) > 0)
                                $SQLInsert->values->add($element->name, file_get_contents($_FILES[$name]['tmp_name']));
                            break;
                        default:
                            $SQLInsert->values->add($element->name, $_POST[$name]);
                            break;
                    }
                }
            }

            return $SQLInsert->execute();
        }
    }

    public function getGroup(string $column): array|false
    {
        if(array_key_exists($column, $this->elements)){
            /** @var PDOStatement $statement */
            $statement = $this->db->prepare("SELECT $column FROM $this->name GROUP BY $column");
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } else return false;
    }

    public function copyByTables(Table $fromTable, Table $toTable){
        // Get Values
        $select = new Select($fromTable);
        foreach ($fromTable->elements as $element){
            $select->column->add($fromTable->name, $element->name);
        }
        $list = $select->execute();

        // Insert Values
        foreach ($list as $item){
            $insert = new Insert($toTable);
            foreach($toTable->elements as $element){
                $insert->values->add($element->name, $item[$element->namespace.'.'.$element->name]);
            }

            try{
                $insert->execute();
            } catch (\PDOException $e) {
                if($e->getCode() === "23000") {
                    // try update
                    $update = new Update($toTable);
                    foreach($toTable->elements as $element){
                        if(in_array($element->name, $toTable->primary)){
                            $update->where->add($toTable->name, $element->name, $item[$element->namespace.'.'.$element->name]);
                        } else {
                            $update->set->add($toTable->name, $element->name, $item[$element->namespace.'.'.$element->name]);
                        }
                    }
                    $update->execute();
                }
            }
        }
    }
}

function replace_first_str($search_str, $replacement_str, $src_str){
    return (false !== ($pos = strpos($src_str, $search_str))) ? substr_replace($src_str, $replacement_str, $pos, strlen($search_str)) : $src_str;
}
