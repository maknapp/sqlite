<?php

namespace Maknapp\SQLite;

use PDO;
use PDOStatement;

class Select
{
    public const PARAM_FETCH = 0;
    public const PARAM_FETCHALL  = 1;

    private Table $table;
    public QueryColumn $column;
    public QueryWhere $where;
    public QueryOrderBy $orderBy;
    public QueryLimit $limit;

    public function __construct(Table &$table)
    {
        $this->table = &$table;
        $this->column = new QueryColumn();
        $this->where = new QueryWhere();
        $this->orderBy = new QueryOrderBy();
        $this->limit = new QueryLimit();
    }

    public function query(): string
    {
        $columns = $this->column->query($this->table->name, $this->table->joins);

        $query = "SELECT $columns FROM {$this->table->name}";

        if(sizeof($this->table->joins) > 0){
            foreach ($this->table->joins as $join){
                $query .= " ".$join->query();
            }
        }

        $query .= $this->where->query($this->table->name, $this->table->joins);
        $query .= $this->orderBy->query($this->table->name, $this->table->joins);
        $query .= $this->limit->query();

        return $query;
    }

    public function execute($fetch = Select::PARAM_FETCHALL): array|false
    {
        /** @var PDOStatement $statement */
        $statement = $this->table->getDb()->prepare($this->query());
        $this->bindValues($statement);
        $statement->execute();

        if($fetch === Select::PARAM_FETCH) $result = $statement->fetch(PDO::FETCH_ASSOC);
        else $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $this->convertData($result, $fetch);
    }

    private function bindValues(PDOStatement $statement)
    {
        foreach ($this->where->get() as $item){
            if($item['type'] !== 2){
                $element = $this->table->getElement($item['table'].'.'.$item['column']);
                $paramName = $item['table'].'_'.$item['column'];
                switch (get_class($element)){
                    case "Maknapp\SQLite\ElementInteger":
                        if($item['type'] == QueryWhere::PARAM_EQUAL) {
                            $statement->bindValue($paramName, $item['value'], PDO::PARAM_INT);
                        } else {
                            $statement->bindValue($paramName, $item['value']);
                        }
                        break;
                    case "Maknapp\SQLite\ElementBoolean":
                        $statement->bindValue($paramName, isset($item['value']) ? 1 : 0, PDO::PARAM_BOOL);
                        break;
                    case "Maknapp\SQLite\ElementText":
                    case "Maknapp\SQLite\ElementReal":
                        $statement->bindValue($paramName, $item['value']);
                        break;
                    case "Maknapp\SQLite\ElementBlob":
                        $statement->bindValue($paramName, $item['value'], PDO::PARAM_LOB);
                        break;
                    default:
                        throw new ErrorException(get_class($this)." datatype not found");
                }
            }
        }
    }

    private function convertData(array|false $data, $mode): array|false
    {
        if($mode === Select::PARAM_FETCH){
            $result = [];
            if($data === false) $result = false;
            else{
                foreach ($data as $key => $value){
                    if(!is_numeric($key)){
                        $result[replace_first_str('_', '.', $key)] = $value;
                    }
                }
            }

            return $result;
        } else {
            $result = [];
            if($data === false) $result = false;
            else{
                foreach ($data as $row){
                    $rowResult = [];
                    foreach ($row as $key => $value){
                        if(!is_numeric($key)){
                            $rowResult[replace_first_str('_', '.', $key)] = $value;
                        }
                    }
                    $result[] = $rowResult;
                }
            }

            return $result;
        }
    }
}
