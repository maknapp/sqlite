<?php

namespace Maknapp\SQLite;

class QueryOrderBy
{
    public bool $asc = true;
    private array $columns = [];

    public function __construct()
    {

    }

    public function add(string $table, string $column){
        $this->columns[] = ['table' => $table, 'column' => $column];
    }

    public function query(string $table, array $joins): string
    {
        $tables = [$table];
        foreach ($joins as $join){
            if(!in_array($join->tableA, $tables)) $tables[] = $join->tableA;
            if(!in_array($join->tableB, $tables)) $tables[] = $join->tableB;
        }

        $list = [];
        foreach ($this->columns as $column){
            if(in_array($column['table'], $tables)){
                $list[] = $column['table'].'.'.$column['column'];
            }
        }

        if(sizeof($list) > 0) return " ORDER BY ".implode(', ', $list)." ".($this->asc ? "ASC" : "DESC");
        else return '';
    }
}
