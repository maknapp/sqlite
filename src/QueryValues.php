<?php

namespace Maknapp\SQLite;



class QueryValues
{
    private array $columns = [];
    public function __construct()
    {

    }

    public function add(string $column, $value){
        $this->columns[] = ['column' => $column, 'value' => $value];
    }

    public function get(){
        return $this->columns;
    }

    public function query(): string
    {
        $columns = [];
        $values = [];
        foreach ($this->columns as $column){
            $columns[] = $column['column'];
            $values[] = ':'.$column['column'];
        }

        if(sizeof($columns) > 0) return " (".implode(', ', $columns).") VALUES (".implode(', ', $values).")";
        else return '';
    }
}
