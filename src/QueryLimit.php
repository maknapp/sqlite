<?php

namespace Maknapp\SQLite;

class QueryLimit
{
    public ?int $length = null;
    public ?int $start = null;

    public function __construct()
    {

    }

    public function query(): string
    {
        if(!is_null($this->length)){
            if(!is_null($this->start)) return " LIMIT $this->start, $this->length";
            else return " LIMIT $this->length";
        } else return '';
    }
}
