<?php

namespace Maknapp\SQLite;

use Maknapp\Dialog\Field;
use Maknapp\Dialog\FieldReal;
use Maknapp\Dialog\FieldSelect;
use SimpleXMLElement;

class ElementReal extends Element
{
    private  float $default = 0;
    private ?float $min = null;
    private ?float $max = null;
    private ?float $step = null;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['default'])) $this->default = (float) $attributes["default"];
        if(!is_null($attributes['min'])) $this->min = (float) $attributes["min"];
        if(!is_null($attributes['max'])) $this->max = (float) $attributes["max"];
        if(!is_null($attributes['step'])) $this->step = (float) $attributes["step"];

        if(!is_null($attributes['modus']) && (string) $attributes['modus'] === 'select') $this->dialogField = new FieldSelect($namespace, $node);
        else $this->dialogField = new FieldReal($namespace, $node);
    }

    public function getDialogField(array $values): Field
    {
        $this->dialogField->value = !is_null($this->value) ? $this->value : $this->default;

        return $this->dialogField;
    }
}
