<?php

namespace Maknapp\SQLite;



class QueryWhere
{
    public const PARAM_EQUAL = 0;
    public const PARAM_LIKE  = 1;
    public const PARAM_IN    = 2;

    public const PARAM_AND = 0;
    public const PARAM_OR = 1;

    private array $columns = [];
    public function __construct()
    {

    }

    public function add(string $table, string $column, $value, int $type = QueryWhere::PARAM_EQUAL, int $logical = QueryWhere::PARAM_AND){
        $this->columns[] = ['table' => $table, 'column' => $column, 'value' => $value, 'type' => $type, 'logical' => $logical];
    }

    public function get(){
        return $this->columns;
    }

    public function query(string $table, array $joins): string
    {
        $tables = [$table];
        foreach ($joins as $join){
            if(!in_array($join->tableA, $tables)) $tables[] = $join->tableA;
            if(!in_array($join->tableB, $tables)) $tables[] = $join->tableB;
        }

        $list = [];
        foreach ($this->columns as $column){
            if(in_array($column['table'], $tables)){
                if($column['type'] === 2){
                    $list[] = [
                        'query' => $column['table'].'.'.$column['column']." IN ({$column['value']})",
                        'logical' => $column['logical']
                    ];

                } else {
                    $list[] = [
                        'query' => $column['table'].'.'.$column['column'].' '.(['=', 'LIKE', 'IN'])[$column['type']].' :'.$column['table'].'_'.$column['column'],
                        'logical' => $column['logical']
                    ];
                }
            }
        }

        if(sizeof($list) > 0){
            $query = "";
            foreach ($list as $key => $element){
                if($query === ""){
                    $query .= " WHERE";
                }
                $query .= " {$element['query']}";
                if($key !== sizeof($list) - 1){
                    if ($element['logical'] === QueryWhere::PARAM_AND) {
                        $query .= " AND";
                    } else {
                        $query .= " OR";
                    }
                }
            }
            return $query;
        }
        else return '';
    }
}
