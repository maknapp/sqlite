<?php

namespace Maknapp\SQLite;

use Maknapp\Dialog\Field;
use Maknapp\Dialog\FieldFile;
use SimpleXMLElement;

class ElementBlob extends Element
{
    private ?string $mime = null;

    public function __construct(string $namespace, SimpleXMLElement $node)
    {
        parent::__construct($namespace, $node);

        $attributes = $node->attributes();

        if(!is_null($attributes['mime'])) $this->mime = (string) $attributes["mime"];

        $this->dialogField = new FieldFile($namespace, $node);
    }

    public function getDialogField(array $values): Field
    {
        return $this->dialogField;
    }
}
