<?php

namespace Maknapp;

use PDO;

trait DBTrait
{
    protected PDO $db;

    public function setDb(PDO $db)
    {
        $this->db = $db;
    }

    public function getDb(): PDO
    {
        return $this->db;
    }

}