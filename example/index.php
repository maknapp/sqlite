<?php
namespace Maknapp\SQLite;

use Maknapp\User;
use PDO;

require_once('../vendor/autoload.php');
require('DBTrait.php');
require('Group.php');
require('User.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Table SQLite</title>
    <script src="../vendor/maknapp/dialog/script/Dialog.js"></script>

    <link href="http://static.fabian-maknapp.de/css/ci.css" rel="stylesheet">
    <link href="http://static.fabian-maknapp.de/css/dialog.css" rel="stylesheet">
    <link href="http://static.fabian-maknapp.de/font/LibreBaskerville/LibreBaskerville.css" rel="stylesheet">
    <link href="http://static.fabian-maknapp.de/font/Montserrat/Montserrat.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body style="display: initial; overflow: auto;">
<input type="number" id="id" placeholder="id" value="1">
<button onclick="(function(){dialog.load('api.dialog.php?id='+document.getElementById('id').value)})()">Edit User By Id</button>
<button onclick="(function(){dialog.load('api.dialog.php')})()">New User</button>
<script type="application/javascript">
    let dialog = new Maknapp.Dialog();
</script>
<h2>Data test</h2>
<?php
$table = new User();
$table->setDb(new PDO("sqlite:example.sqlite"));

// SELECT
$select = new Select($table);

$select->column->add('user', 'username');
$select->column->add('user', 'password');
$select->column->add('userGroup', 'name');
$select->column->add('user', 'Count(*)');

$select->where->add('user', 'username', "'f.maknapp','a.dmin'", QueryWhere::PARAM_IN);

$select->orderBy->add('user', 'id');
$select->orderBy->add('user', 'username');
$select->orderBy->asc = false;

$select->limit->length = 2;

var_dump($select->query());
var_dump($select->execute());

// UPDATE
$update = new Update($table);

$update->set->add('user', 'username','fma');

$update->where->add('user', 'id', 1);

var_dump($update->query());
//var_dump($update->execute());

// INSERT
$insert = new Insert($table);

$insert->values->add('username', 'test');
$insert->values->add('groupId', '1');

var_dump($insert->query());
//var_dump($insert->execute());

// DELETE
$delete = new Delete($table);

$delete->where->add('user', 'id', 6);

var_dump($delete->query());
//var_dump($delete->execute());
?>
</body>
</html>