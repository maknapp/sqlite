<?php

use Maknapp\Dialog\Result;
use Maknapp\User;

require_once('../vendor/autoload.php');
require('DBTrait.php');
require('User.php');
require('Group.php');

$table = new User();
$table->setDb(new PDO("sqlite:example.sqlite"));

$result = $table->saveForm(isset($_POST['user_id']));

$dialogResult = new Result();
$dialogResult->code = $result === false ? 0 : 1;

header('content-type: application/json');
print $dialogResult->getJSON();
