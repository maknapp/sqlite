<?php

use Maknapp\User;

require_once('../vendor/autoload.php');
require('DBTrait.php');
require('User.php');
require('Group.php');

$table = new User();
$table->setDb(new PDO("sqlite:example.sqlite"));

$values = isset($_GET['id']) ? $table->getByKey($_GET['id']) : [];

$dialog = $table->getDialogForm($values);
$dialog->title = isset($_GET['id']) ? "Edit User" : "New User";
$dialog->action = "api.user.save.php";

if($values === false){
    http_response_code(404);
} else {
    header('content-type: application/json');
    echo $dialog->getJSON();
}
