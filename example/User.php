<?php

namespace Maknapp;

use Maknapp\SQLite\Select;
use Maknapp\SQLite\Table;

class User extends Table
{
    use DBTrait;

    public function __construct()
    {
        parent::__construct("table.user");
    }

    public function getByKey(int $id): array|false
    {
        $select = new Select($this);
        foreach ($this->elements as $element){
            $select->column->add($this->name, $element->name);
        }
        $select->where->add($this->name, $this->primary[0], $id);

        return $select->execute(Select::PARAM_FETCH);
    }

    public function deleteByKey(int $id): bool
    {
        $delete = new Select($this);
        $delete->where->add($this->name, $this->primary[0], $id);

        return $delete->execute();
    }
}