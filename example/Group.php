<?php

namespace Maknapp;

use Maknapp\SQLite\Table;

class Group extends Table
{
    use DBTrait;

    public function __construct()
    {
        parent::__construct("table.group");
    }
}